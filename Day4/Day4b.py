import itertools

def anagramCount(filename):
    validpair = 0
    anagramcount = 0
    f = open(filename+".txt","r")
    for line in f:
        passphrase = line.split()
        combination = list(itertools.combinations(passphrase,2))
        for pair in combination:
            if sorted(pair[0]) != sorted(pair[1]):
               validpair += 1
        if validpair == len(combination):
            anagramcount += 1
        ##reset valid pair to 0 for next list
        validpair = 0       
    print anagramcount
    return
