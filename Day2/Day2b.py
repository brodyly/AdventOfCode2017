import csv
import codecs
import itertools

def checksum():
    sum = 0
    with codecs.open('test.csv', 'r', encoding="utf-8-sig") as spreadsheet:
        csv_reader = csv.reader(spreadsheet,delimiter=',')
        
        for row in csv_reader:
            row = map(int, row)
            pairs = list(itertools.permutations(row,2))
            for number in pairs:
                remainder = int(number[0]) % int(number[1])
                if remainder == 0:
                    sum+= number[0] / number[1]
        print sum
    return
