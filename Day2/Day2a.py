import csv
import codecs

def checksum():
    sum = 0
    with codecs.open('test.csv', 'r', encoding="utf-8-sig") as spreadsheet:
        csv_reader = csv.reader(spreadsheet,delimiter='\t')
        
        for row in csv_reader:
            row = map(int, row)
            sum += int(max(row)) - int(min(row))
        print sum
    return
