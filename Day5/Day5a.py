def maze():
    f = open("input.txt","r")
    instructions = f.read().splitlines()
    instructions = map(int, instructions)
    #always start at first element
    index = 0
    steps = 0
    while index < len(instructions):
        prev_index = index
        #replace old index with the element ofthe array at the index
        index += instructions[index]
        #increment old element by 1
        instructions[prev_index] += 1
        steps += 1
    print steps
    return
    
    
    
    
