def maze():
    f = open("input.txt","r")
    instructions = f.read().splitlines()
    instructions = map(int, instructions)
    #always start at first element
    index = 0
    steps = 0
    while index < len(instructions):
        prev_index = index
        #replace old index with the element ofthe array at the index
        index += instructions[index]
        #increment/decrease offset rules
        if instructions[prev_index] >= 3:
            instructions[prev_index] -= 1
        else:
            instructions[prev_index] += 1
        steps += 1
    print steps
    return
    
    
    
    
