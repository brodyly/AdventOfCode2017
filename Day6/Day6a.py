import copy

def memoryAlloc():
    blocks = [4,1,15,12,0,9,9,5,5,8,7,3,14,5,12,3]
    index = 0
    cycleCount = 1 #starting off at 1 count due to the first iteration.
    config = []
    while True:
        # Grab the first index of the mem holding largest block
        index = blocks.index(max(blocks))
        noBlocks = blocks[index]
        blocks[index] = 0
        for j in range(noBlocks):
            # Wrap index around with modulo
            index = (index + 1) % len(blocks)
            blocks[index] += 1
        #have to use copy to avoid appending a reference to blocks
        newconfig = copy.deepcopy(blocks)
        if blocks in config:
            break;
        else:
            cycleCount += 1
            config.append(newconfig)
    print cycleCount
    return
